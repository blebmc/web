## WIP

https://blebmc.gitlab.io/web/


## How to contribute

Writing pages or making graphics for this would be super helpful.

**The most basic way to help is to make a plain text document and write your content in that. You can then change the extension to .md and upload it to the docs/ folder. It will then appear on the site.**

All the pages are written in [Markdown](https://www.markdownguide.org/basic-syntax/) with optional HTML.

If you wanna work a bit more closely and get to see a live preview as you work, you can follow these steps:

1. Make sure you have [Python](https://www.python.org/) installed on your computer
2. Open a command prompt or terminal and type `pip install mkdocs`
3. Clone this repository. (You can just download the whole thing as a .zip and extract it somewhere.)
4. Open a command prompt or terminal from that directory and type `mkdocs serve`
5. Go to http://localhost:8000 to see a live preview of the site
6. Work on your own pages or edit existing ones
7. Push your changes to this repository
