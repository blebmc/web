The Blebs are a community following [Hrry](http://twitch.tv/Hrry), a twitch.tv streamer. <br />
We have worked on a few Minecraft projects, and this site serves as a way to collect those projects together.

Follow the links below to discover our projects :)

[Minecraft maps](./maps.md){: .md-button .md-button--primary }
[Resource pack](https://modrinth.com/resourcepack/blebpack){: .md-button .md-button--primary }
[Data pack](https://www.curseforge.com/minecraft/customization/bleb-resources-songs-and-tales){: .md-button .md-button--primary }

Hrry and some of his chat members formed a development group, which operated from 2021-2024.<br />
They provided unique multiplayer Minecraft experiences via their servers.

[Blebi Studios](https://web.archive.org/web/20240324134746/https://bleb.io/){: .md-button .md-button--primary }