# Bleb Race for the Wool
A team-based, PVP friendly map with lots of replayability.<br />
Race across 4 huge areas to gather wool and meet objectives faster than your opponents.<br />
Play time is around 3 hours for a first playthrough.<br />
`Built for Java Edition 1.16.5`<br />
[Get it on CurseForge](https://www.curseforge.com/minecraft/worlds/race-for-the-wool-bleb-edition){: .md-button }
[Get it on Planet Minecraft](https://www.planetminecraft.com/project/race-for-the-wool-bleb-edition){: .md-button }
[Player Guide](./rftw1.md){: .md-button .md-button--primary }

For singleplayer speedrunners, a special single-lane version has been made.<br />
Features functions to reset the map and an integrated timer.<br />
`Built for Java Edition 1.16.5`<br />
[Get it on CurseForge](https://www.curseforge.com/minecraft/worlds/bleb-race-for-the-wool-speedrun-edition){: .md-button }