# Bleb Race for the Wool - Player Guide

**This guide aims to familiarise players with the map and learn how to play.**

This guide assumes you have formed two teams of a few players each.<br />
The teams are referred to as <span style="color:#00aaaa">Warped Team</span> and <span style="color:#aa0000">Crimson Team</span>.

 For info about *how to start the game*, jump to [this section.](#the-hub)
 <br />

---

## Overview and rules

Race for the Wool is a game mode focused on a reasonably simple capture-and-retrieve model.<br />
In this game, you as a team are tasked with collecting 5 different coloured wool blocks.<br />
Along the way you must also meet 5 secondary objectives, which are explained in this guide.<br />

Your environment consists of a narrow lane. The opposing team plays seperately to you on another mirrored lane, and your lanes are separated by free space. You may not cross the gap yourself except to fall from the world and respawn.

This map is PVP friendly. You are encouraged to try to slow down your opponents by interfering with their progression. This could be by using ranged weapons across the lane gap to hit them directly, destroy their terrain, or activate traps and explosives.

Chests containing items critical for game progression cannot be destroyed by players. Taking one of these items from a chest will cause it to respawn, so you have an unlimited supply of these items when you find them.

Wool is found in special wool rooms, made of coloured concrete and glass. Inside the rooms is a wool fountain. All wool rooms have an unlimited supply of wool.

When collecting wool you can carry as many as you want. You can place them on the ground. When you die, the wool will disappear from your inventory and you must collect more.

When you pick up a piece of wool, you become a *carrier*. When this happens you are no longer able to use teleporters and must traverse the whole map to bring your wool back to the wool monument. Therefore, you are encouraged to make a clear path for your wool carrier to move to the monument safely.

---


!!! info
    Because of the map's large scale, it's recommended to keep checking the world map as you move through the map.<br />
    In-game the world map is placed next to your wool monument and in every checkpoint room.<br />
    [View world map online](../images/rftw1-world-map.png){ .md-button }
    [View scoreboard key online](../images/rftw1-sb-noalpha.png){ .md-button }

---

## Basic guide to the map and the objectives

The game takes place over 5 main areas, each with their own wool to collect and an objective to complete.

![Spider SE](../images/small/rftw1-spider-SE.png){: align=center }

The starting area is the *Spider Caves*. This is the most organic and non-linear section of the map. In order to locate the wool and obsidian you will need to study the opposite lane carefully to find a route between the bedrock. It might be beneficial to split up your team to focus on different objectives.

!!! note
    You don't have to collect each wool in order. You can leave this wool and come back later when you're more prepared, if you like.

Collecting the obsidian will allow you to progress out of the overworld and into the nether, to reach more wools and more gear. You will be given one diamond pickaxe in the obsidian room, and it will only be able to mine obsidian and crying obsidian.

---

![Temple SW](../images/small/rftw1-temple-SW.png){: align=center }

Next is the *Desert Temple*. Studying the opposite lane will give you the information you need to reach the wool, deep under the temple. Under the temple you can find the first respawn checkpoint room, shown with emerald blocks on the map.

Just before the temple building, you can find the crying obsidian. You will need the crying obsidian to progress to the stronghold area later.

Reaching the end of this area, you will find one more respawn point and a teleporter to the wool monument. To activate the portal to progress to the nether, place your obsidian block in the portal to complete it.

!!! warning
    Remember, you cannot use a teleporter when carrying wool.

---

![Nether SE](../images/small/rftw1-nether-SE.png){: align=center }

*Literal Hell* is a compact labyrinth of nether biomes. The crying obsidian nether portal in the middle of this area will allow you to progress to the stronghold. To activate the portal place your crying obsidian block in the portal to complete it.

There are multiple routes through this area, some shorter than others. Study the opposite team's lane carefully to look for any shortcuts you can take, but be aware that shorter routes may have tougher mobs waiting to strike.

At the end of this area is the main boss mob of this map: King Squishy. Behind their throne you will find the blaze powder, one of the required ingredients to craft the Eye of Ender, essential in unlocking the final area of the map.

Beyond the throne room is a passage to reach the red wool.

!!! tip
    Defeating King Squishy will turn all of the lava in the entire area into magma blocks. Therefore, killing King Squishy is a good way to make your return with wools easier.

---

![Stronghold SW](../images/small/rftw1-stronghold-SW.png){: align=center }

In the *Stronghold*, you will find the second and final required ingredient to craft an Eye of Ender: the Ender Pearls. This will allow you progress to the final area.

You can also find the yellow wool here. Reaching it will be a challenge, you will have to be creative in order to reach it without perishing in the lava below it.

A checkpoint respawn room is available in the middle of this area.

---

![End SE](../images/small/rftw1-end-SE.png){: align=center }

In the final area, *The End Moon*, you will find the strongest enemies and toughest terrain, but you will also find the best gear and weapons. In the crashed ship at the entrance to the moon you will find chests with elytra and fireworks for your team. These will help you on the long journey to the wool monument with the purple wool.

The purple wool is at the centre of the moon.

---

## Catch up mechanics

This map features a *catch up* system. When one team gains a wool, the other team is granted access to a catch up chest next to their wool monument. Inside the chest are items that will assist the team in gaining that wool.

For example, if <span style="color:#00aaaa">Warped Team</span> pick up a green wool, the <span style="color:#aa0000">Crimson Team</span> gains access immediately to the green wool catch up chest. Inside the chest are tools and items that will make fighting spiders and removing cobwebs easier.

These mechanics are designed to help close the gap between the winning and losing team and keep games very competitive and close.

The catch up chests automatically open when unlocked. They can be found at the lower part of the hub building.<br />
<br />
![Catch up chests](../images/rftw1-catchup.png){: align=center }
---

## The scoreboard 

![Scoreboard key](../images/rftw1-sb.png){: align=center }

## The hub

When you load up the map, your team assembles in the top section of the hub.

To select a team, step on the coloured concrete powder at either end of the hub (marked team select).

When your team is ready, you can pull the lever and stand in the 'trough'.<br />
When both teams levers are pulled, the trough will open and the game will begin.

![Hub](../images/rftw1-hub.png){: align=center }

## Speedrunning

For singleplayer speedrunners, a special single-lane version of the map has been developed.<br />
It features functions to reset the map, a new scoreboard and an integrated timer.<br />
You can download this version [here.](https://www.curseforge.com/minecraft/worlds/bleb-race-for-the-wool-speedrun-edition)<br />